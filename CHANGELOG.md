# Version history


## [v1.1](https://gitlab.com/chkoupinator/real.discount_dataminer/-/releases/v1.1) Language Detection & Anti-Tracking Update (2020-03-25)
#### New Features:
* The title of the offer will now be saved and cached, it will also be shown if you don't select the short printing mode.
* Optional language detection using [the `langdetect` library](https://pypi.org/project/langdetect/), will still work if not installed.
* Linksynergy trackers will now be detected and removed from the links, making them shorter and helping you protect your privacy.
#### Improvements:
* Improved formatting of remaining time, better management of cases with unknown remaining time and of expired coupons.
* Made the lines contain even less character by removing unnecessary spaces and rewording things.
* Invalid cache should not cause any crashes regardless of any upcoming change, it will simply redownload the cache when keys are broken or cannot be found.
* Improved performance by moving the sorting phase below the potential exclusions when skipping expired or unlimited offers.


### [v1.0.1](https://gitlab.com/chkoupinator/real.discount_dataminer/-/releases/v1.0.1) Formatting Improvements (2020-03-22)
#### Improvements:
* Changed short print formatting to have less characters per line (helpful to copy paste lines in char restricted chats for example).


## [v1.0](https://gitlab.com/chkoupinator/real.discount_dataminer/-/releases/v1.0) Initial Release (2020-03-16)

