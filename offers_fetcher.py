import threading
import time

import requests
import re


MAX_THREADS = 10
RETRY_SLEEP = 1

offer_regex = re.compile("href=\"([^\"]*/offer/[^\"]*)\"")


def get_offers_urls(html_page):
    return offer_regex.findall(html_page)


def get_expiring_soon_pages_single_thread(verbose=False):
    pages = list()
    all_offers = list()
    while True:
        if verbose:
            print(f"fetching page number {len(pages)+1}")
        page = requests.get(f"https://www.real.discount/expiring-soon-coupons/page/{len(pages)+1}")
        offers = get_offers_urls(page.text)
        if len(offers) == 0:
            break
        if verbose:
            print(f"{len(offers)} offers found!")
        pages.append(page)
        all_offers.append(offers)
    return pages, all_offers


class FetcherThread(threading.Thread):
    threads = list()  # type: [FetcherThread]

    def __init__(self, _id, verbose=False):
        threading.Thread.__init__(self)
        FetcherThread.threads.append(self)
        self.id = _id
        self.done = False
        print(f"Thread {_id} starting")

        self.all_offers = list()
        self.all_offers_flat = list()
        self.pages = list()
        self.verbose = verbose

    def run(self):
        while True:
            page_number = self.id + MAX_THREADS * len(self.pages)
            if self.verbose:
                print(f"Thread {self.id} : Fetching page number {page_number}")
            page = None
            while page is None:
                try:
                    page = requests.get(f"https://www.real.discount/expiring-soon-coupons/page/{page_number}")
                except requests.exceptions.ConnectionError:
                    print(f"Thread {self.id} had a ConnectionError, retrying in {RETRY_SLEEP} sec")
                    time.sleep(RETRY_SLEEP)
            offers = get_offers_urls(page.text)
            if len(offers) == 0:
                if self.verbose:
                    print(f"Thread {self.id} done! ({len(self.pages)} pages downloaded !)")
                    self.done = True
                break
            if self.verbose:
                print(f"Thread {self.id} : {len(offers)} offers found!")
            self.pages.append(page)
            self.all_offers.append(offers)
            self.all_offers_flat.extend(offers)

    @classmethod
    def all_done(cls):
        for thread in cls.threads:  # type: FetcherThread
            if not thread.done:
                return False
        return True

    @classmethod
    def get_all_offers(cls) -> [str]:
        all_offers = list()
        for thread in cls.threads:  # type: FetcherThread
            all_offers.extend(thread.all_offers_flat)
        return all_offers

    @classmethod
    def get_offers_urls_from_expiring_soon_pages(cls, verbose=False) -> [str]:
        for i in range(MAX_THREADS):
            cls(i, verbose).start()

        while not cls.all_done():
            time.sleep(1)

        return cls.get_all_offers()



