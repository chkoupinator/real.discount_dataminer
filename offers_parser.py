import time
import requests
import re
import threading
from urllib3.exceptions import ProtocolError
from urllib.parse import unquote


MAX_THREADS = 10
RETRY_SLEEP = 1


coursefacts_extractor = re.compile('<div class="coursefacts"[ ]?>([^\0]*?)</div>')
fact_extractor = re.compile('>(.*?)</span>')
time_extractor = re.compile('data-expire="(\\d*)"')
link_extractor = re.compile('<a[^>]*?href="([^"]+)"[^>]*>Get Coupon')
price_extractor = re.compile('<h5 class="price">([^<]*)')
title_extractor = re.compile('<title>([^<]+)</title>')
linksynergy_remover = re.compile('(http://click.linksynergy.com.*)https://')


def smol_time():
    return time.strftime("[%H:%M:%S] ", time.localtime())


class Offer:

    def __init__(self, offer_url, price, title, students, rating, length,
                 times_open, expiration_time, remaining, course_url):
        self.offer_url = offer_url
        self.price = price
        self.title = title
        self.students = students
        self.rating = rating
        self.length = length
        self.times_open = times_open
        self.expiration_time = expiration_time
        self.remaining = remaining
        self.course_url = course_url

    def __str__(self):
        return f"__offer___________________________________________________________________________________________\n" \
               f"  url : {self.offer_url}\n" \
               f"  title : {self.title}" \
               f"  price : {self.price}\n" \
               f"  coursefacts :\n" \
               f"    enrolled : {self.students}\n" \
               f"    rating : {self.rating}\n" \
               f"    length : {self.length}\n" \
               f"    page opened : {self.times_open} times\n" \
               f"  expiration : {time.strftime('%d-%m-%Y %H:%M', time.localtime(self.expiration_time))}\n" \
               f"  remaining : {self.remaining}\n" \
               f"  coupon url : {self.course_url}\n"

    def to_dict(self):
        return {
            "offer_url": self.offer_url,
            "title": self.title,
            "price": self.price,
            "students": self.students,
            "rating": self.rating,
            "length": self.length,
            "times_open": self.times_open,
            "expiration_time": self.expiration_time,
            "remaining": self.remaining,
            "course_url": self.course_url
        }

    def __eq__(self, other):
        if isinstance(other, Offer):
            return other.offer_url == self.offer_url and other.rating == self.rating and \
                   other.course_url == self.course_url and other.expiration_time == self.expiration_time

    @classmethod
    def from_raw_html(cls, offer_url, raw_html):
        price = price_extractor.findall(raw_html)[0]
        coursefacts = coursefacts_extractor.findall(raw_html)
        students, rating, length, times_open, expiration_time, remaining = str(), str(), str(), str(), int(), str()
        for fact in coursefacts:
            if "users" in fact:
                f = fact_extractor.findall(fact)
                students = f[0] if len(f) > 0 else "Unknown"
            elif "star" in fact:
                f = fact_extractor.findall(fact)
                rating = f[0] if len(f) > 0 else "Unknown"
            elif "clock" in fact:
                f = fact_extractor.findall(fact)
                length = f[0] if len(f) > 0 else "Unknown"
            elif "eye" in fact:
                f = fact_extractor.findall(fact)
                times_open = f[0] if len(f) > 0 else "Unknown"
            elif "Expiration" in fact:
                f = time_extractor.findall(fact)
                expiration_time = int(f[0]) if len(f) > 0 else 0
            elif "Remaining" in fact:
                remaining = "Unknown" if "Unknown" in fact else "Known (somehow?)"
        course_url = unquote(link_extractor.findall(raw_html)[0])                            # type: str
        if "http://click.linksynergy.com" in course_url:
            course_url = course_url.replace(linksynergy_remover.findall(course_url)[0], '')
        title = title_extractor.findall(raw_html)[0].replace("- Real Discount", "")
        return cls(offer_url, price, title, students, rating, length, times_open, expiration_time, remaining,
                   course_url)

    @classmethod
    def from_dict(cls, d):
        return cls(offer_url=d["offer_url"], price=d["price"], title=d["title"], students=d["students"],
                   rating=d["rating"], length=d["length"], times_open=d["times_open"],
                   expiration_time=d["expiration_time"], remaining=d["remaining"], course_url=d["course_url"])


def always_download(url, arg2=None):
    return None


class OfferParser(threading.Thread):
    threads = list()
    is_in_cache_check = always_download

    def __init__(self, offers_urls: [str]):
        threading.Thread.__init__(self)
        OfferParser.threads.append(self)
        print(f"Thread {len(OfferParser.threads)} starting")
        self.offers_urls = offers_urls
        self.total_offers = len(offers_urls)  # since we'll remove urls after downloading
        self.offers = list()

    @classmethod
    def downloads_left(cls):
        return sum(len(t.offers_urls) for t in cls.threads)

    def run(self):
        while len(self.offers_urls) > 0:
            url = self.offers_urls[0]

            cache_check = OfferParser.is_in_cache_check(url)
            if isinstance(cache_check, Offer):
                self.offers.append(cache_check)
                self.offers_urls.pop(0)
                continue

            try:
                response = requests.get(url)
            except ProtocolError:
                time.sleep(RETRY_SLEEP)
                continue

            # process the offer
            self.offers.append(Offer.from_raw_html(self.offers_urls.pop(0), response.text))

    @classmethod
    def parse_these_urls(cls, offers_urls, verbose=False) -> [Offer]:
        threads_to_run = len(offers_urls) if len(offers_urls) < MAX_THREADS else MAX_THREADS  # to avoid empty threads
        for i in range(threads_to_run):
            print(offers_urls[i::threads_to_run])
            cls(offers_urls[i::threads_to_run])

        for thread in cls.threads:
            thread.start()

        while cls.downloads_left() > 0:
            if verbose:
                print(f"{smol_time()} Pages left to download : {cls.downloads_left()} / {len(offers_urls)}")
            time.sleep(1)

        offers = list()

        for thread in cls.threads:  # type: OfferParser
            offers.extend(thread.offers)

        return offers

