# Real.Discount Dataminer


## Introduction
This is a small script I made to ease my own access to the [Real.Discount website](https://www.real.discount) using only the requests library.

## Setup

### Getting the prerequisites:
The only prerequisite for this is to install the [requests library](https://pypi.org/project/requests/) that handles HTTP requests.

To install it :

**Linux :** ```pip3 install requests```

**Windows / Mac :** ```pip install requests```

If you don't have python3 installed you can check [their website](https://www.python.org).

A version of python >= 3.5 is required although this was made using 3.6.

#### Optional library for language detection:
[The langdetect library](https://pypi.org/project/langdetect/) can be installed to separate english courses from other ones.

**Linux :** ```pip3 install langdetect```

**Windows / Mac :** ```pip install langdetect```

### Getting the repository:
You can clone the repository using git or download it as a compressed file (zip or tar). All it needs is for the 3 python files to be in the same folder as *main.py*. The cache.json file can also be downloaded optionally to make the first run much faster by skipping the 1500+ offers that are forever free.

### Running it:
Simply use `python3 main.py` with a terminal open in the directory containing *main.py* if you're on Linux or `python main.py` if you're on Windows or Mac.


## How to use it

After running it you will simply be prompted with a bunch of questions. 

[y/N] means that if you type anything starting with a lowercase or uppercase 'y' such as "yes" will be considered as **True**, otherwise it will default to **False**

It is recommended that you avoid downloading the pages anew every time, instead only refresh the cache file once you suspect that there might be new offers (courses) added to the website.

The recommended sort method is by rating, as it priorities the courses that are most likely to be valuable to you.

## Upcoming Features

- More Sorting Methods
- A way to filter courses that contain specific words only
- Getting data from udemy or eduonix directly once the coupon is found
- Adding the title (done) and the description of the courses to the course cached data


## Added Features

- **2020-03-25:** Language Detection (through the use of the `langdetect` library)
- **2020-03-25:** Removal of the `click.linksynergy.com` redirect tracking, links to the courses will be extracted and directly used as the course URL instead

Your suggestions are always welcome!
