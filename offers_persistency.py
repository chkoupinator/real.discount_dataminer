import json
from offers_parser import Offer


def save(all_offers, name="cache"):
    with open(f"{name}.json", 'w+') as file:
        json.dump(list(o.to_dict() for o in all_offers), file, indent=2)
        file.truncate()


def load(name="cache"):
    with open(f"{name}.json", 'r') as file:
        all_offers_dicts = json.load(file)

    all_offers = list()
    for d in all_offers_dicts:
        all_offers.append(Offer.from_dict(d))
    return all_offers


loaded_cache = None


def check_if_offer_in_cache(url, cache_name="cache"):
    global loaded_cache
    if loaded_cache is None:
        with open(f"{cache_name}.json", 'r') as file:
            loaded_cache = json.load(file)

    for offer in loaded_cache:  # type: dict
        if offer["offer_url"] == url:
            return Offer.from_dict(offer)
    return None

