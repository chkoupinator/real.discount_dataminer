import sys
import time
import requests

import offers_fetcher
import offers_parser
import offers_persistency

LANG_DETECTION = True
try:
    import langdetect
except ModuleNotFoundError:
    LANG_DETECTION = False


VERBOSE = True


offers = list()  # type: [offers_parser.Offer]

if input("Do you want to refresh the cache? [y/N]").lower().startswith("y"):
    offers_urls = offers_fetcher.FetcherThread.get_offers_urls_from_expiring_soon_pages(VERBOSE)

    if not input("Do you want to force refresh even already cached offers? [y/N]").lower().startswith("y"):
        offers_parser.OfferParser.is_in_cache_check = offers_persistency.check_if_offer_in_cache

    offers = offers_parser.OfferParser.parse_these_urls(offers_urls, VERBOSE)
    offers_persistency.save(offers)


elif input("Do you want to fetch a single page? [y/N]"):

    page = requests.get(f"https://www.real.discount/expiring-soon-coupons/page/{1}")
    offers_urls = offers_fetcher.get_offers_urls(page.text)

    print(offers_urls)
    offers = offers_parser.OfferParser.parse_these_urls(offers_urls, VERBOSE)

else:

    try:
        offers = offers_persistency.load()
    except FileNotFoundError:
        print("cache not found, please run the program again and load a cache", file=sys.stderr)
    except KeyError:
        print("cache follows an old structure, it has to be refreshed now", file=sys.stderr)
        time.sleep(0.1)
        input("(press enter to continue)")
        offers_urls = offers_fetcher.FetcherThread.get_offers_urls_from_expiring_soon_pages(VERBOSE)
        offers = offers_parser.OfferParser.parse_these_urls(offers_urls, VERBOSE)
        offers_persistency.save(offers)


sorting = str()
while True:
    sorting = input("How do you want to sort the offers?\n"
                    "1- By Expiration Time\n"
                    "2- By Rating\n"
                    "enter - Unsorted")
    if sorting in ["1", "2", ""]:
        break


def sorting_value(offer: offers_parser.Offer):
    if sorting == "1":
        return time.time() - offer.expiration_time
    elif sorting == "2":
        return offer.rating if offer.rating != "Unknown" else 0


if sorting != "":
    offers.sort(key=sorting_value, reverse=True)

show_forever_free = input("Do you want to show offers with no time limit? [y/N]").lower().startswith("y")

show_expired = input("Do you want to show expired coupons? [y/N]").lower().startswith("y")


english_offers = list()
other_offers = list()

for offer in offers:

    if not show_forever_free and offer.expiration_time == 0:
        continue

    if offer.expiration_time - time.time() < 0 and not show_expired:
        continue

    if offer in english_offers or offer in other_offers:
        continue

    if LANG_DETECTION:
        for lang in langdetect.detect_langs(offer.title):
            if lang.lang == 'en' and lang.prob > 0.5:
                english_offers.append(offer)
                break
        else:
            other_offers.append(offer)
    else:
        english_offers.append(offer)


dont_show_mini_mode = input("Do you want to show offers in a smaller mode? [Y/n]").lower().startswith("n")


print("rating / 5 stars (time left), link to the course")


offers_to_show = english_offers + [None] + other_offers
other_offers_reached = False


for offer in offers_to_show:    # type: offers_parser.Offer

    if offer is None:
        other_offers_reached = True
        print("Offers that are probably not in english :")
        continue

    if dont_show_mini_mode:
        print(offer)
        continue

    time_left_str = str()

    if offer.expiration_time == 0:
        time_left_str = "unknown time left"
    else:
        time_left = offer.expiration_time - time.time()
        if time_left < 0:
            time_left_str = "EXPIRED"
        else:
            if time_left > 86400:
                time_left_str += f"{int(time_left//86400)}d "
            if time_left > 3600:
                time_left_str += f"{int((time_left//3600)%24)}h "
            if not time_left > 86400:
                time_left_str += f"{int((time_left//60)%60)}m"
            time_left_str = f"{time_left_str.strip(' ')} left"
    print(f"{offer.rating.replace(' ', '')} ({time_left_str.strip(' ')}): {offer.course_url}")

    if other_offers_reached:
        if not LANG_DETECTION:
            break
        r = langdetect.detect_langs(offer.title)
        print(f"language : {', '.join(f'{r[i].lang}:{r[i].prob:.0%}' for i in range(3 if len(r) > 3 else len(r)))}")

